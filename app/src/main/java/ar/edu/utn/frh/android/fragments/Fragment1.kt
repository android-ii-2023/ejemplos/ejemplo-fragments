package ar.edu.utn.frh.android.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment

class Fragment1 : Fragment(R.layout.layout_1) {

    private lateinit var navegador: Navegador

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navegador = context as Navegador
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.setOnClickListener {
            navegador.irA_Pantalla2()
        }
    }
}