package ar.edu.utn.frh.android.fragments

import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(), Navegador {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        when (resources.configuration.orientation) {
            Configuration.ORIENTATION_LANDSCAPE -> configurarLandscape()
            Configuration.ORIENTATION_PORTRAIT -> configurarPortrait()
        }
    }

    private fun configurarLandscape() {
        val venimosDeLandscape =
            supportFragmentManager.fragments.size == 2 &&
                    supportFragmentManager.findFragmentById(R.id.fragment_2) != null

        if (venimosDeLandscape) {
            return
        }

        // Borramos todo
        val transaction = supportFragmentManager.beginTransaction()
        supportFragmentManager.fragments.forEach {
            transaction.remove(it)
        }
        transaction.commit()

        // Recreamos
        supportFragmentManager.beginTransaction()
            .add(R.id.el_fragment, Fragment1())
            .add(R.id.fragment_2, Fragment2())
            .commit()
    }

    private fun configurarPortrait() {
        if (supportFragmentManager.fragments.isEmpty()) {
            // Primer inicio de la Activity
            supportFragmentManager.beginTransaction()
                .add(R.id.el_fragment, Fragment1())
                .commit()
            return
        }

        val fragment2 = supportFragmentManager.fragments.firstOrNull { it is Fragment2 }
        if (fragment2 != null) {
            if (supportFragmentManager.findFragmentById(R.id.el_fragment) == fragment2) {
                // Fragment2 viene de portrait
                return
            } else {
                // Fragment2 viene de landscape
                supportFragmentManager.beginTransaction().remove(fragment2).commit()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.el_fragment, Fragment2())
                    .addToBackStack(null)
                    .commit()
            }
        }

        // Si llegamos acá sólo hay Fragment1. Siga siga
    }

    override fun irA_Pantalla2() {
        var fragment2 = supportFragmentManager.findFragmentById(R.id.fragment_2)

        if (fragment2 == null) {
            fragment2 = Fragment2()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.el_fragment, fragment2)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }
}